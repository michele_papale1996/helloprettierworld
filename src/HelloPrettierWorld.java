import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class HelloPrettierWorld {

    public static void main(String[] args) {
        HelloPrettierWorld h = new HelloPrettierWorld();
        h.test1();
    }

    public void test1() {
        EntityT entityT = null;
        EntityT prova = null;
        prova.print(entityT.getEvent());
    }

    public void test() {





        String s =
            "UPDATE RS_DYNAMICLIST_ELEMENT SET statusid = 7, updatets = ? WHERE itemid IN (:EVENTS_TO_DELETED) AND eventid IN (:EVENTS_TO_DELETED) AND statusid = 1";

        List<EntityT> toRemove = new ArrayList<>();
        List<String> params = new ArrayList<>();
        toRemove.add(new EntityT("itemid", "eventid"));
        toRemove.add(new EntityT("itemid1", "eventid1"));
        StringBuilder placeholders = new StringBuilder();
        for (EntityT elementToRemove : toRemove) {
            if (placeholders.length() == 0) {
                placeholders.append("?");
            } else {
                placeholders.append(",?");
            }
            params.add((elementToRemove).getId());
        }

        for (EntityT elementToRemove : toRemove) {
            params.add((elementToRemove).getEvent());
        }

        s = s.replace(":EVENTS_TO_DELETED", placeholders.toString());

        System.out.println(params);
        System.out.println(s);
    }

    private class EntityT {
        String id;
        String event;

        public EntityT(String id, String event) {
            this.id = id;
            this.event = event;
        }

        public void print(String toPrint) {
            System.out.println(toPrint);
        }

        public String getEvent() {
            return event;
        }

        public String getId() {
            return id;
        }
    }
}
